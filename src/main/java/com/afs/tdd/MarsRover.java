package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        }
        if (command.equals(Command.Left)) {
            turnLeft();
        }
        if (command.equals(Command.Right)) {
            turnRight();
        }

    }

    public void executeCommands(List<Command> commands) {
        // is equal to commands.stream().forEach(command -> executeCommand(command))
        // is equal to commands.forEach(command -> executeCommand(command))

        // this refers this class MarsRover;
        //two colons mean two command in lambda exepression command -> executeCommand(command)
        commands.forEach(this::executeCommand);  
                                                
    }

    private void turnRight() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.South);
                break;
            case South:
                location.setDirection(Direction.West);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
        }

    }

    private void turnLeft() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.West);
                break;
            case West:
                location.setDirection(Direction.South);
                break;
            case South:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.North);
                break;

        }
    }

    private void move() {
        switch (location.getDirection()) {
            case North:
                location.setCoordinateY(location.getCoordinateY() + 1);
                break;
            case West:
                location.setCoordinateX(location.getCoordinateX() - 1);
                break;
            case South:
                location.setCoordinateY(location.getCoordinateY() - 1);
                break;
            case East:
                location.setCoordinateX(location.getCoordinateX() + 1);
                break;
        }
    }

    public Location getLocation() {
        return location;
    }
}
