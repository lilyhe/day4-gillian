# day4-Gillian

## Objective: 
Code review for yesterday's homework. It was until the code review that I found that three tests didn't pass because I was just running tests of each step but not for the whole project.

I learned to write unit tests and TDD. TDD has three steps: write a test that fails, make the code work and eliminate redundancy. When writing unit tests, there are three parts:  given, when, and then.

## Reflective: 
I am very happy that I can quickly turn the theory about TDD into actual code.

## Interpretive: 
When writing test cases, there will be a lot of repeated code, which makes me do a lot of repeated practices, deepening my understanding of TDD. Therefore the learning effect is better than before.

## Decisional: 
Use more shortcut keys to improve coding speed. 

Be alert when there is a lot of duplicate code in the implementation code. It's probably redundant
